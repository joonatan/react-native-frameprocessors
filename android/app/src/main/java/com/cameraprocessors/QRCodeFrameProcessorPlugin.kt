package com.cameraprocessors

import android.util.Log
import androidx.camera.core.ImageProxy
import com.facebook.react.bridge.WritableNativeArray
import com.facebook.react.bridge.WritableNativeMap
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.common.InputImage
import com.mrousavy.camera.frameprocessor.FrameProcessorPlugin

class QRCodeFrameProcessorPlugin : FrameProcessorPlugin("scanQRCodes") {
    override fun callback(imageProxy: ImageProxy, params: Array<Any>): WritableNativeArray? {
        Log.d("QRCode", "start frame processor")

        val options = BarcodeScannerOptions.Builder()
            .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
            .build()

        val tasks: ArrayList<Task<List<Barcode>>> = ArrayList<Task<List<Barcode>>>()

        val mediaImage = imageProxy.image
        if (mediaImage != null) {
            val image = InputImage.fromMediaImage(mediaImage, imageProxy.imageInfo.rotationDegrees)
            // Pass image to an ML Kit Vision API
            // ...
            val scanner = BarcodeScanning.getClient(options)
            tasks.add(scanner.process(image));

            try {
                val barcodes = ArrayList<Barcode>()
                for (task in tasks) {
                    barcodes.addAll(Tasks.await(task))
                }
                val array = WritableNativeArray()
                for (barcode in barcodes) {
                    array.pushMap(convertBarcode(barcode))
                }
                return array
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        return null
    }

    private fun convertBarcode(barcode: Barcode): WritableNativeMap? {
        val map = WritableNativeMap()
        /*
        val boundingBox: Rect? = barcode.boundingBox
        if (boundingBox != null) {
            map.putMap("boundingBox", convertToMap(boundingBox))
        }
        val cornerPoints: Array<Point>? = barcode.cornerPoints
        if (cornerPoints != null) {
            map.putArray("cornerPoints", convertToArray(cornerPoints))
        }
        */
        val displayValue = barcode.displayValue
        if (displayValue != null) {
            map.putString("displayValue", displayValue)
        }
        val rawValue = barcode.rawValue
        if (rawValue != null) {
            map.putString("rawValue", rawValue)
        }
        //map.putMap("content", convertContent(barcode))
        map.putInt("format", barcode.format)
        return map
    }
}