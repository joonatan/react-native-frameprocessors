/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-reanimated';

import React from 'react';
import type {Node} from 'react';
import {StyleSheet, Text, Pressable, View} from 'react-native';

import {
  Camera,
  useCameraDevices,
  useFrameProcessor,
} from 'react-native-vision-camera';

import {scanQRCodes, scanOCR} from './src/qr-code-scanner';

const App: () => Node = () => {
  const devices = useCameraDevices('wide-angle-camera');
  const device = devices.back;
  const [cameraPermission, setCameraPermission] =
    React.useState<CameraPermissionStatus>();
  const [mode, setMode] = React.useState('qr');

  React.useEffect(() => {
    Camera.getCameraPermissionStatus().then(setCameraPermission);
  }, []);

  const ocrFrameProcessor = useFrameProcessor(frame => {
    'worklet';
    const val = scanOCR(frame);
    console.log('ocr processed: ', val);
    const texts = val.blocks.map(x => x.text);
    console.log(texts);
  }, []);

  const qrFrameProcessor = useFrameProcessor(frame => {
    'worklet';
    const val = scanQRCodes(frame);
    console.log('qr code processed: ', val);
  }, []);

  //console.log('device: ', device)
  const toggleMode = () => {
    if (mode === 'ocr') {
      console.log('toggle mode QR code');
      setMode('qr');
    } else {
      console.log('toggle mode OCR');
      setMode('ocr');
    }
  };

  if (cameraPermission === null) {
    return <Text>No camera permissions</Text>;
  }
  if (!device) {
    return <Text>Loading Camera</Text>;
  }
  return (
    <>
      <Text style={styles.title}>{mode} Scanner</Text>
      <Camera
        style={{ flex: 1 }}
        //style={StyleSheet.absoluteFill}
        device={device}
        isActive={true}
        frameProcessor={mode === 'ocr' ? ocrFrameProcessor : qrFrameProcessor}
        //frameProcessor={frameProcessor}
        frameProcessorFps={1}
      />
      <View style={styles.buttonContainer}>
        <Pressable style={styles.button} onPress={toggleMode}>
          <Text>Switch {mode === 'ocr' ? 'QR' : 'OCR'}</Text>
        </Pressable>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    marginBottom: 8,
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  button: {
    fontSize: 16,
    padding: 4,
    textAlign: 'center',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
