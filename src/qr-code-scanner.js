/**
 * @format
 * @flow strict-local
 */
//import type {Frame} from 'react-native-vision-camera';

/**
 * Scans QR codes.
 */
//export function scanQRCodes(frame: Frame): string[] {
export function scanQRCodes(frame) {
  'worklet';
  return __scanQRCodes(frame);
}

export function scanOCR(frame) {
  'worklet';
  return __scanOCR(frame);
}
