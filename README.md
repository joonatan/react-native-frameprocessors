# Frameprocessors
React Native Frameprocessors for react-native-vision-camera
https://github.com/mrousavy/react-native-vision-camera

A test / example for implementing QR code and OCR with Android.

Problem with the frameprocessor plugins is that they all have to have same version of native libraries. So pulling a plugin for QR codes created half a year ago and a recent OCR plugin will not work. They both need to be up to date with same libraries.

Another thing is that vision-camera releases don't yet work with recent React Native so it has to be from Git.

## TODO
- TODO: iOS versions
- TODO: make into a plugin (that can be pulled with npm)
- TODO: add image labeling plugin also
